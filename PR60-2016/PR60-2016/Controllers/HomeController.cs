﻿using PR60_2016.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR60_2016.Controllers
{
    public class HomeController : Controller
    {
        Dictionary<string, Aranzman> aranzmans = new Dictionary<string, Aranzman>();
        public ActionResult Index()
        {
            aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");

            //DictionarySort(aranzmans);
            //ViewBag.Aranzmans = aranzmanSort;
            ViewBag.Aranzmans = aranzmans;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

       
    }
}