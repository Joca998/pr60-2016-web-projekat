﻿using PR60_2016.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR60_2016.Controllers
{
    public class AranzmanController : Controller
    {
        Dictionary<string, Aranzman> aranzmans = new Dictionary<string, Aranzman>();
        Dictionary<string, Smestaj> smestaji = new Dictionary<string, Smestaj>();
        Dictionary<string, Aranzman> aranzmanSort = new Dictionary<string, Aranzman>();
        Dictionary<string, Smestaj> smestajSort = new Dictionary<string, Smestaj>();
        Dictionary<string, SmestajnaJedinica> jedinica = new Dictionary<string, SmestajnaJedinica>();
        Dictionary<string, SmestajnaJedinica> jedinicasort = new Dictionary<string, SmestajnaJedinica>();
        public static Dictionary<string, Aranzman> sort = new Dictionary<string, Aranzman>();
        public static Dictionary<string, Smestaj> sorts = new Dictionary<string, Smestaj>();
        public static Dictionary<string, SmestajnaJedinica> sortsj = new Dictionary<string, SmestajnaJedinica>();
        // GET: Aranzman
        public ActionResult Index()
        {
            aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");

            DictionarySort(aranzmans);
            ViewBag.Aranzmans = aranzmanSort;
            DictionarySortS(smestaji);
            ViewBag.Smestaji = smestajSort;
            DictionarySortSJ(jedinica);
            ViewBag.SmestajnaJedinica = jedinicasort;
            return View();
        }

        public ActionResult AddAranzman()
        {
            Dictionary<string, Smestaj> smestaji = new Dictionary<string, Smestaj>();
            Dictionary<string, MestoNalazenja> mesta = new Dictionary<string, MestoNalazenja>();
            smestaji = ReadWrite.ReadSmestaj("~/App_Data/Smestaj.txt");
            mesta = ReadWrite.ReadPlace("~/App_Data/MestoNalazenja.txt");
            ViewBag.Smestaji = smestaji;
            ViewBag.Places = mesta;

            string today = DateTime.Today.ToString("yyyy-MM-ddThh:mm");
            ViewBag.Today = today;

            if (TempData["status"] != null)
            {
                ViewBag.Status = "Smestaj vec postoji!";
                TempData.Remove("status");
            }

            if (TempData["remaining"] != null)
            {
                ViewBag.Remaining = TempData["remaining"].ToString();
                TempData.Remove("remaining");
            }


            return View();
        }

        [HttpPost]
        public ActionResult AddAranzman(AranzmanHelp help)
        {
            Dictionary<string, Aranzman> aranzmans = new Dictionary<string, Aranzman>();
            Dictionary<string, Smestaj> smestaji = new Dictionary<string, Smestaj>();
            Dictionary<string, MestoNalazenja> places = new Dictionary<string, MestoNalazenja>();

            aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");
            smestaji = ReadWrite.ReadSmestaj("~/App_Data/Smestaj.txt");
            places = ReadWrite.ReadPlace("~/App_Data/MestoNalazenja.txt");

            Smestaj s = new Smestaj();
            MestoNalazenja mn = new MestoNalazenja();

            Aranzman newAranzman = new Aranzman();



            foreach (var place in places)

            {
                if (help.Place == place.Value.Place)
                {
                    mn.Id = place.Key;
                    mn.Place = place.Value.Place;

                }

            }

            foreach (var smes in smestaji)
            {
                if (help.NazivSmestaja == smes.Value.Naziv)
                {
                    s.Id = smes.Key;
                    s.Naziv = smes.Value.Naziv;
                    s.TipS = smes.Value.TipS;
                    s.Bazen = smes.Value.Bazen;
                    s.OsobeSaInvaliditetom = smes.Value.OsobeSaInvaliditetom;
                    s.Wifi = smes.Value.Wifi;
                    s.SpaCentar = smes.Value.SpaCentar;
                    s.BrojZvezdica = smes.Value.BrojZvezdica;


                }
            }


            newAranzman.Adresa = mn;
            newAranzman.Id = help.Id;
            newAranzman.Naziv = help.Naziv;
            newAranzman.TipA = help.TipA;
            newAranzman.TipP = help.TipP;
            newAranzman.Lokacija = help.Lokacija;
            newAranzman.DateStart = help.DateStart;
            newAranzman.DateEnd = help.DateEnd;
            newAranzman.Vreme = help.Vreme;
            newAranzman.MaxBrojPutnika = help.MaxBrojPutnika;
            newAranzman.Opis = help.Opis;
            newAranzman.ProgramPutovanja = help.ProgramPutovanja;
            newAranzman.Poster = help.Poster;
            newAranzman.Smestaj = s;





            Korisnik us = (Korisnik)Session["US"];
            ReadWrite.WriteAranzman(newAranzman, "~/App_Data/Aranzmans.txt", us.Id);

            return RedirectToAction("Index", "Aranzman");
        }




        private void DictionarySort(Dictionary<string, Aranzman> dict)
        {
            var dictSort = from objDict in dict orderby objDict.Value.DateStart descending select objDict;

            aranzmanSort = dictSort.ToDictionary(t => t.Key, t => t.Value);

        }


        private void DictionarySortS(Dictionary<string, Smestaj> dict)
        {
            var dictSort = from objDict in dict orderby objDict.Value.Naziv descending select objDict;

            smestajSort = dictSort.ToDictionary(t => t.Key, t => t.Value);

        }

        private void DictionarySortSJ(Dictionary<string, SmestajnaJedinica> dict)
        {
            var dictSort = from objDict in dict orderby objDict.Value.MaxGostiju descending select objDict;

            jedinicasort = dictSort.ToDictionary(t => t.Key, t => t.Value);

        }

        public ActionResult ActivateAranzman()
        {
            aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzman.txt");

            ViewBag.Aranzmans = aranzmans;

            return View();
        }


        public ActionResult EditAranzman(string id)
        {
            Dictionary<string, Smestaj> smestaji = new Dictionary<string, Smestaj>();
            Dictionary<string, MestoNalazenja> places = new Dictionary<string, MestoNalazenja>();
            aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");
            places = ReadWrite.ReadPlace("~/App_Data/MestoNalazenja.txt");
            Aranzman ar = new Aranzman();



            foreach (var ars in aranzmans)
            {
                if (ars.Value.Id == id)
                {
                    ar = aranzmans[id];
                }
            }

            if (TempData["remaining"] != null)
            {
                ViewBag.Remaining = TempData["remaining"].ToString();
                TempData.Remove("remaining");
            }
            if (TempData["status"] != null)
            {
                ViewBag.Status = "Smestaj je zauzet";
                TempData.Remove("status");
            }

            string dates = ar.DateStart.ToString("yyyy-MM-dd");
            string datee = ar.DateEnd.ToString("yyyy-MM-dd");


            string today = DateTime.Today.ToString("yyyy-MM-dd");

            ViewBag.Today = today;
            ViewBag.DateS = dates;
            ViewBag.DateE = datee;
            ViewBag.Aranzman = ar;
            ViewBag.Places = places;
            ViewBag.Smestaji = smestaji;
            return View();
        }

        [HttpPost]
        public ActionResult EditAranzman(AranzmanHelp help)
        {
            Dictionary<string, Aranzman> aranzmans = new Dictionary<string, Aranzman>();
            Dictionary<string, Smestaj> smestaji = new Dictionary<string, Smestaj>();
            Dictionary<string, MestoNalazenja> places = new Dictionary<string, MestoNalazenja>();

            aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");
            smestaji = ReadWrite.ReadSmestaj("~/App_Data/Smestaj.txt");
            places = ReadWrite.ReadPlace("~/App_Data/MestoNalazenja.txt");

            Smestaj s = new Smestaj();
            MestoNalazenja mn = new MestoNalazenja();

            Aranzman newAranzman = new Aranzman();



            foreach (var place in places)

            {
                if (help.Place == place.Value.Place)
                {
                    mn.Id = place.Key;
                    mn.Place = place.Value.Place;

                }

            }

            foreach (var smes in smestaji)
            {
                if (help.NazivSmestaja == smes.Value.Naziv)
                {
                    s.Id = smes.Key;
                    s.Naziv = smes.Value.Naziv;
                    s.TipS = smes.Value.TipS;
                    s.Bazen = smes.Value.Bazen;
                    s.OsobeSaInvaliditetom = smes.Value.OsobeSaInvaliditetom;
                    s.Wifi = smes.Value.Wifi;
                    s.SpaCentar = smes.Value.SpaCentar;
                    s.BrojZvezdica = smes.Value.BrojZvezdica;


                }
            }


            newAranzman.Adresa = mn;
            newAranzman.Id = help.Id;
            newAranzman.Naziv = help.Naziv;
            newAranzman.TipA = help.TipA;
            newAranzman.TipP = help.TipP;
            newAranzman.Lokacija = help.Lokacija;
            newAranzman.DateStart = help.DateStart;
            newAranzman.DateEnd = help.DateEnd;
            newAranzman.Vreme = help.Vreme;
            newAranzman.MaxBrojPutnika = help.MaxBrojPutnika;
            newAranzman.Opis = help.Opis;
            newAranzman.ProgramPutovanja = help.ProgramPutovanja;
            newAranzman.Poster = help.Poster;
            newAranzman.Smestaj = s;


            aranzmans[help.Id] = newAranzman;

            ReadWrite.WriteEditAranzman(aranzmans, "~/App_Data/Aranzmans.txt");

            return RedirectToAction("Index", "Aranzman");
        }


        public ActionResult MenadzerAranzmans()
        {
            Korisnik user = (Korisnik)Session["US"];
            Dictionary<string, Aranzman> aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");
            Dictionary<string, Aranzman> menadzer = new Dictionary<string, Aranzman>();

            foreach (var ar in aranzmans)
            {
                if (ar.Value.MenadzerId == user.Id)
                {
                    menadzer[ar.Key] = aranzmans[ar.Key];
                }
            }

            DictionarySort(menadzer);
            ViewBag.Menadzer = aranzmanSort;

            return View();
        }

        public ActionResult AllAranzmans()
        {
            aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");

            TempData["send"] = aranzmans;
            sort = aranzmans;
            return RedirectToAction("SearchAranzman", "Aranzman");
        }

        public ActionResult SearchAranzman()
        {


            if (TempData["send"] != null)
            {
                ViewBag.Find = TempData["send"];
            }

            if (TempData["Error"] != null)
            {
                ViewBag.Error = "No aranzman found for search parameters";
                TempData.Remove("Error");
            }

            if (TempData["Errorp"] != null)
            {
                ViewBag.Errorp = "You must enter at least one search parameter";
                TempData.Remove("Errorp");
            }


            return View();
        }

        [HttpPost]
        public ActionResult SearchAranzman(SearchAranzmanHelp sHelp)
        {
            aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");
            Dictionary<string, Aranzman> find = new Dictionary<string, Aranzman>();

            if (sHelp.DateMin1.ToString() == "01/01/0001 00:00:00" && sHelp.DateMin2.ToString() == "01/01/0001 00:00:00" && sHelp.DateMax1.ToString() == "01/01/0001 00:00:00" && sHelp.DateMax2.ToString() == "01/01/0001 00:00:00" && sHelp.TipA == TipAranzmana.UNDEFINED && sHelp.TipP == TipPrevoza.UNDEFINED && sHelp.Naziv == null)
            {
                TempData["Errorp"] = "You must enter at least one search parameter";
                RedirectToAction("SearchAranzman", "Aranzman");
            }
            else
            {
                foreach (var ar in aranzmans)
                {
                    if ((ar.Value.Naziv == sHelp.Naziv || sHelp.Naziv == null) && (ar.Value.TipA == sHelp.TipA || sHelp.TipA == TipAranzmana.UNDEFINED) && (ar.Value.DateStart >= sHelp.DateMin1 || sHelp.DateMin1.ToString() == "01/01/0001 00:00:00") && (ar.Value.DateStart <= sHelp.DateMin2 || sHelp.DateMin2.ToString() == "01/01/0001 00:00:00") && (ar.Value.DateEnd >= sHelp.DateMax1 || sHelp.DateMax1.ToString() == "01/01/0001 00:00:00") && (ar.Value.DateEnd <= sHelp.DateMax2 || sHelp.DateMax2.ToString() == "01/01/0001 00:00:00"))
                    {
                        find[ar.Key] = aranzmans[ar.Key];
                    }
                }


                if (find.Count == 0)
                {
                    TempData["Error"] = "No aranzmans found for search parameters";
                }
                else
                {
                    TempData["send"] = find;
                    sort = find;
                }
            }


            return RedirectToAction("SearchAranzman", "Aranzman");
        }

        [HttpPost]
        public ActionResult SortAranzman(SortHelp type)
        {
            Dictionary<string, Aranzman> aranzmans = new Dictionary<string, Aranzman>();
            aranzmans = sort;

            string tip = type.SortType;

            switch (tip)
            {
                case "nameaz":

                    var dictSort = from objDict in aranzmans orderby objDict.Value.Naziv ascending select objDict;

                    aranzmanSort = dictSort.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = aranzmanSort;
                    break;

                case "nameza":

                    var dictSort1 = from objDict in aranzmans orderby objDict.Value.Naziv descending select objDict;

                    aranzmanSort = dictSort1.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = aranzmanSort;
                    break;

                case "dateascstart":

                    var dictSort2 = from objDict in aranzmans orderby objDict.Value.DateStart ascending select objDict;

                    aranzmanSort = dictSort2.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = aranzmanSort;
                    break;

                case "datedscstart":

                    var dictSort3 = from objDict in aranzmans orderby objDict.Value.DateStart descending select objDict;

                    aranzmanSort = dictSort3.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = aranzmanSort;
                    break;

                case "dateascsend":

                    var dictSort4 = from objDict in aranzmans orderby objDict.Value.DateEnd ascending select objDict;

                    aranzmanSort = dictSort4.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = aranzmanSort;
                    break;

                case "datedscsend":

                    var dictSort5 = from objDict in aranzmans orderby objDict.Value.DateEnd descending select objDict;

                    aranzmanSort = dictSort5.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = aranzmanSort;
                    break;
            }

            return RedirectToAction("SearchAranzman", "Aranzman");
        }

        public ActionResult AddSmestaj()
        {
            Dictionary<string, SmestajnaJedinica> jedinica = new Dictionary<string, SmestajnaJedinica>();
            jedinica = ReadWrite.ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");
            ViewBag.SmestajnaJedinica = jedinica;

            if (TempData["status"] != null)
            {
                ViewBag.Status = "Smestaj vec postoji!";
                TempData.Remove("status");
            }

            if (TempData["remaining"] != null)
            {
                ViewBag.Remaining = TempData["remaining"].ToString();
                TempData.Remove("remaining");
            }


            return View();
        }

        [HttpPost]
        public ActionResult AddSmestaj(Smestaj help)
        {

            Dictionary<string, Smestaj> smestaji = new Dictionary<string, Smestaj>();
            Dictionary<string, SmestajnaJedinica> jedinica = new Dictionary<string, SmestajnaJedinica>();

            smestaji = ReadWrite.ReadSmestaj("~/App_Data/Smestaj.txt");
            jedinica = ReadWrite.ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");


            Smestaj newSmestaj = new Smestaj();
            SmestajnaJedinica newJedinica = new SmestajnaJedinica();

            foreach (var j in jedinica)
            {
                if (help.NazivJedinice == j.Value.Naziv)
                {
                    newJedinica.Id = j.Key;
                    newJedinica.Naziv = j.Value.Naziv;
                    newJedinica.Cena = j.Value.Cena;
                    newJedinica.Ljubimci = j.Value.Ljubimci;
                    newJedinica.MaxGostiju = j.Value.MaxGostiju;



                }
            }


            newSmestaj.Id = help.Id;
            newSmestaj.Naziv = help.Naziv;
            newSmestaj.TipS = help.TipS;
            newSmestaj.Bazen = help.Bazen;
            newSmestaj.OsobeSaInvaliditetom = help.OsobeSaInvaliditetom;
            newSmestaj.SpaCentar = help.SpaCentar;
            newSmestaj.Wifi = help.Wifi;
            newSmestaj.BrojZvezdica = help.BrojZvezdica;
            newSmestaj.Jedinica = newJedinica;





            Korisnik us = (Korisnik)Session["US"];
            ReadWrite.WriteSmestaj(newSmestaj, "~/App_Data/Smestaj.txt", us.Id);

            return RedirectToAction("Index", "Aranzman");
        }



        public ActionResult AranzmanInfo(string id)
        {
            aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");



            Aranzman aranzman = new Aranzman();

            foreach (var ar in aranzmans)
            {
                if (ar.Value.Id == id)
                {
                    aranzman = aranzmans[id];
                }
            }

            ViewBag.Aranzman = aranzman;

            return View();
        }

        public ActionResult SmestajInfo(string id)
        {
            smestaji = ReadWrite.ReadSmestaj("~/App_Data/Smestaj.txt");



            Smestaj smestaj = new Smestaj();

            foreach (var s in smestaji)
            {
                if (s.Value.Id == id)
                {
                    smestaj = smestaji[id];
                }
            }

            ViewBag.Smestaji = smestaj;

            return View();
        }

        public ActionResult PretragaSmestaja()
        {


            if (TempData["send"] != null)
            {
                ViewBag.Find = TempData["send"];
            }

            if (TempData["Error"] != null)
            {
                ViewBag.Error = "Nema smestaja za odredjene parametre";
                TempData.Remove("Error");
            }

            if (TempData["Errorp"] != null)
            {
                ViewBag.Errorp = "Unesite ili naziv ili tip smestaja";
                TempData.Remove("Errorp");
            }


            return View();
        }

        [HttpPost]
        public ActionResult PretragaSmestaja(SmestajHelp sHelp)
        {
            smestaji = ReadWrite.ReadSmestaj("~/App_Data/Smestaj.txt");
            Dictionary<string, Smestaj> find = new Dictionary<string, Smestaj>();



            foreach (var s in smestaji)
            {
                if (sHelp.Naziv != null)
                {
                    if (s.Value.Naziv == sHelp.Naziv)
                    {
                        {
                            find[s.Key] = smestaji[s.Key];
                        }
                    }

                }
                else if (sHelp.TipS != TipSmestaja.UNDEFINED)
                {
                    if (s.Value.TipS == sHelp.TipS)
                    {
                        find[s.Key] = smestaji[s.Key];
                    }
                }
                else if ((s.Value.Naziv == sHelp.Naziv || sHelp.Naziv == null) && (s.Value.TipS == sHelp.TipS || sHelp.TipS == TipSmestaja.UNDEFINED) && ((s.Value.Bazen == sHelp.Bazen) && (((s.Value.Wifi == true) || (s.Value.Wifi == false) && ((s.Value.OsobeSaInvaliditetom == true) || (s.Value.OsobeSaInvaliditetom == false))))) && ((s.Value.Wifi == sHelp.Wifi) && (((s.Value.Bazen == true) || (s.Value.Bazen == false)) && ((s.Value.OsobeSaInvaliditetom == true) || (s.Value.OsobeSaInvaliditetom == false)))))
                {
                    find[s.Key] = smestaji[s.Key];
                }
            }


            if (find.Count == 0)
            {
                TempData["Error"] = "Nema smestaja za upisane parametre";
            }
            else
            {
                TempData["send"] = find;
                sorts = find;
            }



            return RedirectToAction("PretragaSmestaja", "Aranzman");
        }

        [HttpPost]
        public ActionResult SortSmestaj(SortHelp type)
        {
            Dictionary<string, Smestaj> smestaji = new Dictionary<string, Smestaj>();
            smestaji = sorts;

            string tip = type.SortType;

            switch (tip)
            {
                case "nameaz":

                    var dictSort = from objDict in smestaji orderby objDict.Value.Naziv ascending select objDict;

                    smestajSort = dictSort.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = smestajSort;
                    break;

                case "nameza":

                    var dictSort1 = from objDict in smestaji orderby objDict.Value.Naziv descending select objDict;

                    smestajSort = dictSort1.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = smestajSort;
                    break;


            }

            return RedirectToAction("PretragaSmestaja", "Aranzman");
        }

        public ActionResult AllSmestaj()
        {
            smestaji = ReadWrite.ReadSmestaj("~/App_Data/Smestaj.txt");

            TempData["send"] = smestaji;
            sorts = smestaji;
            return RedirectToAction("PretragaSmestaja", "Aranzman");
        }




        public ActionResult AddSmestajnaJedinica(string id)
        {


            if (TempData["status"] != null)
            {
                ViewBag.Status = "Smestajna jedinica vec postoji!";
                TempData.Remove("status");
            }

            if (TempData["remaining"] != null)
            {
                ViewBag.Remaining = TempData["remaining"].ToString();
                TempData.Remove("remaining");
            }

            ViewBag.Smestaj = id;

            return View();
        }

        [HttpPost]
        public ActionResult AddSmestajnaJedinica(SmestajnaJedinica help)
        {

            Dictionary<string, SmestajnaJedinica> jedinica = new Dictionary<string, SmestajnaJedinica>();

            jedinica = ReadWrite.ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");


            SmestajnaJedinica newSmestaj = new SmestajnaJedinica();



            newSmestaj.Id = help.Id;
            newSmestaj.Naziv = help.Naziv;
            newSmestaj.MaxGostiju = help.MaxGostiju;
            newSmestaj.Cena = help.Cena;
            newSmestaj.Ljubimci = help.Ljubimci;
            newSmestaj.Smestajj = help.Smestajj;








            Korisnik us = (Korisnik)Session["US"];
            ReadWrite.WriteSmestajnaJedinica(newSmestaj, "~/App_Data/SmestajnaJedinica.txt", us.Id);

            return RedirectToAction("Index", "Aranzman");
        }
        public ActionResult PretragaSmestajneJedinice()
        {


            if (TempData["send"] != null)
            {
                ViewBag.Find = TempData["send"];
            }

            if (TempData["Error"] != null)
            {
                ViewBag.Error = "Nema smestajne jedinice za odredjene parametre";
                TempData.Remove("Error");
            }

            if (TempData["Errorp"] != null)
            {
                ViewBag.Errorp = "Unesite parametre";
                TempData.Remove("Errorp");
            }


            return View();
        }

        [HttpPost]
        public ActionResult PretragaSmestajneJedinice(SmestajnaJedinicaHelp sHelp)
        {
            jedinica = ReadWrite.ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");
            Dictionary<string, SmestajnaJedinica> find = new Dictionary<string, SmestajnaJedinica>();


            if (sHelp.MaxGostiju1 == 0 && sHelp.MinGostiju == 0 && sHelp.Cena == 0)
            {
                TempData["Errorp"] = "Dodajte parametre";
                RedirectToAction(" PretragaSmestajneJedinice", "Aranzman");
            }
            else
            {
                foreach (var s in jedinica)
                {
                    if ((s.Value.Cena == sHelp.Cena || sHelp.Cena == 0) && (s.Value.MaxGostiju <= sHelp.MaxGostiju1 || sHelp.MaxGostiju1 == 0) && (s.Value.MaxGostiju >= sHelp.MinGostiju || sHelp.MinGostiju == 0) && (s.Value.Ljubimci == sHelp.Ljubimci || sHelp.Ljubimci == false))
                    {
                        find[s.Key] = jedinica[s.Key];
                    }
                }


                if (find.Count == 0)
                {
                    TempData["Error"] = "Nema smestaja za upisane parametre";
                }
                else
                {
                    TempData["send"] = find;
                    sortsj = find;
                }




            }
            return RedirectToAction("PretragaSmestajneJedinice", "Aranzman");
        }
        [HttpPost]
        public ActionResult SortSmestajnaJedinica(SortHelp type)
        {
            Dictionary<string, Smestaj> smestaji = new Dictionary<string, Smestaj>();
            jedinica = sortsj;

            string tip = type.SortType;

            switch (tip)
            {
                case "brojasc":

                    var dictSort = from objDict in jedinica orderby objDict.Value.MaxGostiju ascending select objDict;

                    jedinicasort = dictSort.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = jedinicasort;
                    break;

                case "brojdsc":

                    var dictSort1 = from objDict in jedinica orderby objDict.Value.MaxGostiju descending select objDict;

                    jedinicasort = dictSort1.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = jedinicasort;
                    break;

                case "cenaasc":

                    var dictSort2 = from objDict in jedinica orderby objDict.Value.Cena ascending select objDict;

                    jedinicasort = dictSort2.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = jedinicasort;
                    break;

                case "cenadsc":

                    var dictSort3 = from objDict in jedinica orderby objDict.Value.Cena descending select objDict;

                    jedinicasort = dictSort3.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = jedinicasort;
                    break;

            }

            return RedirectToAction("PretragaSmestajneJedinice", "Aranzman");
        }

        public ActionResult AllSmestajneJedinice()
        {
            jedinica = ReadWrite.ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");

            TempData["send"] = jedinica;
            sortsj = jedinica;
            return RedirectToAction("PretragaSmestajneJedinice", "Aranzman");
        }

        public ActionResult BrisanjeAranzmana(string id)
        {
            Dictionary<string, Aranzman> aranzmans = new Dictionary<string, Aranzman>();
            aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");

            if (aranzmans.ContainsKey(id))
            {
                aranzmans[id].Deleted = true;
            }

            ReadWrite.WriteEditAranzman(aranzmans, "~/App_Data/Aranzmans.txt");

            return RedirectToAction("Index", "Aranzman");
        }

        public ActionResult BrisanjeSmestaja(string id)
        {
            Dictionary<string, Smestaj> smestaji = new Dictionary<string, Smestaj>();
            smestaji = ReadWrite.ReadSmestaj("~/App_Data/Smestaj.txt");

            if (smestaji.ContainsKey(id))
            {
                smestaji[id].Deleted = true;
            }

            ReadWrite.WriteEditSmestaj(smestaji, "~/App_Data/Smestaj.txt");

            return RedirectToAction("Index", "Aranzman");
        }

        public ActionResult ListaJedinica(string id)
        {
            Dictionary<string, SmestajnaJedinica> jedinica = new Dictionary<string, SmestajnaJedinica>();
            Dictionary<string, SmestajnaJedinicaHelp> jedinice = new Dictionary<string, SmestajnaJedinicaHelp>();
            Dictionary<string, Smestaj> smestaj = new Dictionary<string, Smestaj>();
            Dictionary<string, SmestajnaJedinica> jed = new Dictionary<string, SmestajnaJedinica>();



            jedinica = ReadWrite.ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");
            /*smestaj = ReadWrite.ReadSmestaj("~/App_Data/Smestaj.txt");

            foreach (var j in jedinica)
            {
                if (smestaj.ContainsKey(j.Value.Id))
                {
                    SmestajnaJedinicaHelp help = new SmestajnaJedinicaHelp();
                    help.Cena = j.Value.Cena;
                    help.Ljubimci = j.Value.Ljubimci;
                    help.MaxGostiju = j.Value.MaxGostiju;
                    help.Id = j.Value.Id;
                    jedinice.Add(j.Value.Id, help);
                }
            }*/

            foreach (var j in jedinica)
            {
                if (j.Value.Smestajj == id)
                {
                    SmestajnaJedinica help = new SmestajnaJedinica();
                    help.Cena = j.Value.Cena;
                    help.Ljubimci = j.Value.Ljubimci;
                    help.MaxGostiju = j.Value.MaxGostiju;
                    help.Id = j.Value.Id;
                    help.Smestajj = id;
                    help.Zauzeto = j.Value.Zauzeto;

                    jed.Add(help.Id, help);
                }
            }

            ViewBag.Jedinice = jed;

            return View();
        }

        public ActionResult SmestajnaJedinicaInfo(string id)
        {
            jedinica = ReadWrite.ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");



            SmestajnaJedinica smestaj = new SmestajnaJedinica();

            foreach (var s in jedinica)
            {
                if (s.Value.Id == id)
                {
                    smestaj = jedinica[id];
                }
            }

            ViewBag.SmestajnaJedinica = smestaj;

            return View();
        }

        public ActionResult BrisanjeSmestajneJedinice(string id)
        {
            Dictionary<string, SmestajnaJedinica> smestaji = new Dictionary<string, SmestajnaJedinica>();
            jedinica = ReadWrite.ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");

            if (jedinica.ContainsKey(id))
            {
                jedinica[id].Deleted = true;
            }

            ReadWrite.WriteEditSmestajnaJedinica(jedinica, "~/App_Data/SmestajnaJedinica.txt");

            return RedirectToAction("Index", "Aranzman");
        }
        /*
        [HttpPost]
        public ActionResult Rez(Rezervacija rez)
        {
           

            Aranzman ar = new Aranzman();
            Dictionary<string, Aranzman> aranzmans = new Dictionary<string, Aranzman>();
            Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();

            users = ReadWrite.ReadUser("~/App_Data/Users.txt");
            aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");
            foreach (var ara in aranzmans)
            {
                if (ara.Value.Id == rez.Aranzmann)
                {
                    if (ara.Value.Rezervisano==true)
                    {

                        TempData["numeror"] = "Aranzman je rezervisan";
                        return RedirectToAction("AranzmanInfo", new { id = rez.Aranzmann });
                    }
                }
            }





            TempData["rez"] = rez;
            return RedirectToAction("AranzmanInfo", new { id = rez.Aranzmann });
        }
        
        public ActionResult Rezervacija(Rezervacija rez)
        {
            Korisnik user = (Korisnik)Session["US"];
            Dictionary<string, Aranzman> aranzmans = new Dictionary<string, Aranzman>();
            Dictionary<string, Korisnik> dic = new Dictionary<string, Korisnik>();

            aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");



            foreach (var ara in aranzmans)
            {
                if (ara.Value.Id == rez.Aranzmann)
                {
                    if (ara.Value.Rezervisano == false)
                    {
                        
                            ara.Value.Rezervisano = true;
                        

                        dic = ReadWrite.ReadUser("~/App_Data/Users.txt");
                        if (dic.ContainsKey(user.Id))
                        {
                            user.Id = user.Id;
                            user.Rezervacija = dic[user.Id].Rezervacija;
                            user.Aranzmans = dic[user.Id].Aranzmans;
                            user.Role = dic[user.Id].Role;
                            user.LoggedIn = user.LoggedIn;
                            user.Deleted = user.Deleted;

                            user.BirthDate = user.BirthDate;
                            dic[user.Id] = user;
                        }

                    }
                }
            }

            ReadWrite.WriteEdit(dic, "~/App_Data/Users.txt");

            ReadWrite.WriteEditAranzman(aranzmans, "~/App_Data/Aranzmans.txt");

            ReadWrite.WriteRez(rez, "~/App_Data/Rez.txt");
            return RedirectToAction("Index", "Araznamans");
        }*/

        public ActionResult EditSmestaj(string id)
        {
            Dictionary<string, SmestajnaJedinica> jedinica = new Dictionary<string, SmestajnaJedinica>();

            smestaji = ReadWrite.ReadSmestaj("~/App_Data/Smestaj.txt");
            jedinica = ReadWrite.ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");
            Smestaj smestaj = new Smestaj();



            foreach (var s in smestaji)
            {
                if (s.Value.Id == id)
                {
                    smestaj = smestaji[id];
                }
            }

            if (TempData["remaining"] != null)
            {
                ViewBag.Remaining = TempData["remaining"].ToString();
                TempData.Remove("remaining");
            }
            if (TempData["status"] != null)
            {
                ViewBag.Status = "Smestaj je zauzet";
                TempData.Remove("status");
            }


            ViewBag.Smestaji = smestaj;
            return View();
        }

        [HttpPost]
        public ActionResult EditSmestaj(SmestajHelp help)
        {

            Dictionary<string, Smestaj> smestaji = new Dictionary<string, Smestaj>();
            Dictionary<string, SmestajnaJedinica> jedinica = new Dictionary<string, SmestajnaJedinica>();


            smestaji = ReadWrite.ReadSmestaj("~/App_Data/Smestaj.txt");
            jedinica = ReadWrite.ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");

            Smestaj s = new Smestaj();
            SmestajnaJedinica sj = new SmestajnaJedinica();





            foreach (var naziv in jedinica)

            {
                if (help.NazivJedinice == naziv.Value.Naziv)
                {
                    sj.Id = naziv.Key;
                    sj.Naziv = naziv.Value.Naziv;
                    sj.MaxGostiju = naziv.Value.MaxGostiju;
                    sj.Ljubimci = naziv.Value.Ljubimci;
                    sj.Cena = naziv.Value.Cena;

                }

            }



            s.Jedinica = sj;
            s.Id = help.Id;
            s.Naziv = help.Naziv;
            s.TipS = help.TipS;
            s.BrojZvezdica = help.BrojZvezdica;
            s.Bazen = help.Bazen;
            s.OsobeSaInvaliditetom = help.OsobeSaInvaliditetom;
            s.SpaCentar = help.SpaCentar;
            s.Wifi = help.Wifi;


            smestaji[help.Id] = s;

            ReadWrite.WriteEditSmestaj(smestaji, "~/App_Data/Smestaj.txt");

            return RedirectToAction("Index", "Aranzman");
        }


        public ActionResult Rez(string id)
        {
            Korisnik us = (Korisnik)Session["US"];
            Dictionary<string, SmestajnaJedinica> jedinice = new Dictionary<string, SmestajnaJedinica>();

            jedinice = ReadWrite.ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");

            Rezervacija res = new Rezervacija();

            foreach (var j in jedinice)
            {
                if (j.Value.Id == id)
                {

                    res.Jedinicaa = id;
                    res.Status = StatusRezervacije.AKTIVNA;
                    res.Turista = us.Id;
                    j.Value.Zauzeto = true;

                }
            }



            ReadWrite.WriteRez(res, "~/App_Data/Rezervacija.txt");
            ReadWrite.WriteEditSmestajnaJedinica(jedinice, "~/App_Data/SmestajnaJedinica.txt");

            return RedirectToAction("Index", "Aranzman");
        }

        /*
        public ActionResult RezAranzman(string id)
        {
            Korisnik us = (Korisnik)Session["US"];
            Dictionary<string, Aranzman> aranzman = new Dictionary<string, Aranzman>();

            aranzman = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");

            Rezervacija res = new Rezervacija();

            foreach (var j in aranzman)
            {
                if (j.Value.Id == id)
                {

                    res.Aranzmann = id;
                    res.Status = StatusRezervacije.AKTIVNA;
                    res.Turista = us.Id;
                    j.Value.Rezervisano = true;

                }
            }



            ReadWrite.WriteRez(res, "~/App_Data/Rezervacija.txt");
            ReadWrite.WriteEditAranzman(aranzman, "~/App_Data/Aranzmans.txt");

            return RedirectToAction("Index", "Aranzman");
        }

        /*public ActionResult Rezervacija(Rezervacija rez)
        {
            Korisnik user = (Korisnik)Session["US"];
            Dictionary<string, Aranzman> aranzmans = new Dictionary<string, Aranzman>();
            Dictionary<string, Korisnik> dic = new Dictionary<string, Korisnik>();

            aranzmans = ReadWrite.ReadAranzman("~/App_Data/Aranzmans.txt");



            foreach (var ara in aranzmans)
            {
                if (ara.Value.Id == rez.Aranzmann)
                {
                    if (ara.Value.Rezervisano == false)
                    {

                        ara.Value.Rezervisano = true;


                        dic = ReadWrite.ReadUser("~/App_Data/Users.txt");
                        if (dic.ContainsKey(user.Id))
                        {
                            user.Id = user.Id;
                            user.Rezervacija = dic[user.Id].Rezervacija;
                            user.Aranzmans = dic[user.Id].Aranzmans;
                            user.Role = dic[user.Id].Role;
                            user.LoggedIn = user.LoggedIn;
                            user.Deleted = user.Deleted;

                            user.BirthDate = user.BirthDate;
                            dic[user.Id] = user;
                        }

                    }
                }
            }

            ReadWrite.WriteEdit(dic, "~/App_Data/Users.txt");

            ReadWrite.WriteEditAranzman(aranzmans, "~/App_Data/Aranzmans.txt");

            ReadWrite.WriteRez(rez, "~/App_Data/Rez.txt");
            return RedirectToAction("Index", "Araznamans");
        }*/

        public ActionResult MojeRezervacije()
        {
            Dictionary<string, Rezervacija> rez = new Dictionary<string, Rezervacija>();
            Dictionary<string, RezervacijaHelp> mojerez = new Dictionary<string, RezervacijaHelp>();
            Dictionary<string, SmestajnaJedinica> jedinica = new Dictionary<string, SmestajnaJedinica>();


            Korisnik user = (Korisnik)Session["US"];


            rez = ReadWrite.ReadRez("~/App_Data/Rezervacija.txt");
            jedinica = ReadWrite.ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");

            foreach (var r in rez)
            {
                if (r.Value.Turista == user.Id)
                {
                    RezervacijaHelp help = new RezervacijaHelp();

                    help.Id = r.Value.Id;
                    help.Turista = r.Value.Turista;
                    help.Status = r.Value.Status;
                    help.Jedinicaa = r.Value.Jedinicaa;

                    mojerez.Add(r.Value.Id, help);
                }
            }

            ViewBag.Rez = mojerez;
            return View();
        }

        public ActionResult CancelRezervacija(string id)
        {
            Korisnik user = (Korisnik)Session["US"];
            Dictionary<string, Rezervacija> rez = new Dictionary<string, Rezervacija>();
            Dictionary<string, SmestajnaJedinica> jedinica = new Dictionary<string, SmestajnaJedinica>();
            Dictionary<string, Korisnik> dic = new Dictionary<string, Korisnik>();

            jedinica = ReadWrite.ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");

            rez = ReadWrite.ReadRez("~/App_Data/Rezervacija.txt");

            foreach (var j in jedinica)
            {
                foreach (var r in rez)
            {
                    if (r.Value.Id == id)
                    {
                        
                            rez[id].Status = StatusRezervacije.OTKAZANA;
                            user.Canceled += 1;
                        //j.Value.Zauzeto = false;

                            dic = ReadWrite.ReadUser("~/App_Data/Users.txt");
                            if (dic.ContainsKey(user.Id))
                            {
                                user.Id = user.Id;
                                user.Rezervacija = dic[user.Id].Rezervacija;

                                user.Role = dic[user.Id].Role;
                                user.LoggedIn = user.LoggedIn;
                                user.Deleted = user.Deleted;

                                user.BirthDate = user.BirthDate;
                                dic[user.Id] = user;
                            }

                            ReadWrite.WriteEdit(dic, "~/App_Data/Users.txt");
                        
                    }
                }
                }

            
            ReadWrite.WriteEditSmestajnaJedinica(jedinica, "~/App_Data/SmestajnaJedinica.txt");
            ReadWrite.WriteEditRez(rez, "~/App_Data/Rezervacija.txt");
            return RedirectToAction("Index", "Aranzman");


        }
    }
}