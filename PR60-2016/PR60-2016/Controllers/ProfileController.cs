﻿using PR60_2016.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR60_2016.Controllers
{
    public class ProfileController : Controller
    {
        Dictionary<string, Korisnik> korisnici = new Dictionary<string, Korisnik>();
        // GET: Profile
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EditProfile(Korisnik user)
        {
            Korisnik us = (Korisnik)Session["US"];
            korisnici = ReadWrite.ReadUser("~/App_Data/Users.txt");
            if (korisnici.ContainsKey(us.Id))
            {
                user.Id = us.Id;
                user.Role = korisnici[user.Id].Role;
                user.LoggedIn = us.LoggedIn;
                user.Deleted = us.Deleted;
                user.BirthDate = us.BirthDate;
                korisnici[us.Id] = user;
            }
            ReadWrite.WriteEdit(korisnici, "~/App_Data/Users.txt");

            Session["US"] = user;
            return RedirectToAction("Index", "Aranzman");

        }
    }
}