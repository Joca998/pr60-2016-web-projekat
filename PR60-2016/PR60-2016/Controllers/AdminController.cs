﻿using PR60_2016.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR60_2016.Controllers
{
    public class AdminController : Controller
    {
        Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();
        Dictionary<string, Korisnik> usersSort = new Dictionary<string, Korisnik>();
        Dictionary<string, Aranzman> aranzmans = new Dictionary<string, Aranzman>();
        public static Dictionary<string, Korisnik> sort = new Dictionary<string, Korisnik>();
        // GET: Admin
        public ActionResult Index()
        {


            if (TempData["send"] != null)
            {
                ViewBag.Find = TempData["send"];
            }

            if (TempData["Error"] != null)
            {
                ViewBag.Error = "You must enter keyword";
                TempData.Remove("Error");
            }

            if (TempData["Errorp"] != null)
            {
                ViewBag.Errorp = "No users found for keyword";
                TempData.Remove("Errorp");
            }
            if (TempData["FilterErorr"] != null)
            {
                ViewBag.Filter = "Nothing found!";
                TempData.Remove("FilterErorr");
            }

            return View();
        }

        public ActionResult AllUsers()
        {
            users = ReadWrite.ReadUser("~/App_Data/Users.txt");

            TempData["send"] = users;
            sort = users;
            return RedirectToAction("Index", "Admin");
        }

        [HttpPost]
        public ActionResult SearchUsers(SearchUserHelp sHelp)
        {

            
            users = ReadWrite.ReadUser("~/App_Data/Users.txt");
            Dictionary<string, Korisnik> find = new Dictionary<string, Korisnik>();


            if  (sHelp.Ime == null && sHelp.Prezime == null && sHelp.Role == UserRole.UNDEFINED)
            {
                TempData["Errorp"] = "You must enter at least one search parameter";
                RedirectToAction("SearchUsers", "Admin");
            }
            else
            {
                foreach (var us in users)
                {
                    if ((us.Value.Ime == sHelp.Ime || sHelp.Ime == null) && (us.Value.Role == sHelp.Role || sHelp.Role == UserRole.UNDEFINED) && (us.Value.Prezime == sHelp.Prezime || sHelp.Prezime == null))
                    {
                        find[us.Key] = users[us.Key];
                    }
                }


                if (find.Count == 0)
                {
                    TempData["Error"] = "No users found for search parameters";
                }
                else
                {
                    TempData["send"] = find;
                    sort = find;
                }
            }

            return RedirectToAction("Index", "Admin");
        }

        [HttpPost]
        public ActionResult SortUsers(UserSortHelp sortType)
        {
            Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();

            string tip = sortType.Shelp;

            switch (tip)
            {
                case "nameaz":

                    var dictSort = from objDict in sort orderby objDict.Value.Ime ascending select objDict;

                    usersSort = dictSort.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;

                case "nameza":

                    var dictSort1 = from objDict in sort orderby objDict.Value.Ime descending select objDict;

                    usersSort = dictSort1.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;

                case "lastasc":

                    var dictSort2 = from objDict in sort orderby objDict.Value.Prezime ascending select objDict;

                    usersSort = dictSort2.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;

                case "lastdsc":

                    var dictSort3 = from objDict in sort orderby objDict.Value.Prezime descending select objDict;

                    usersSort = dictSort3.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;

                case "roleasc":

                    var dictSort4 = from objDict in sort orderby objDict.Value.Role ascending select objDict;

                    usersSort = dictSort4.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;

                case "roledsc":

                    var dictSort5 = from objDict in sort orderby objDict.Value.Role descending select objDict;

                    usersSort = dictSort5.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;

               
            }

            return RedirectToAction("Index", "Admin");
        }

        public ActionResult AddMenadzer()
        {
            string today = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.Today = today;
            return View();
        }


        [HttpPost]
        public ActionResult AddMenadzer(Korisnik user)
        {
            Dictionary<string, Korisnik> us = (Dictionary<string, Korisnik>)HttpContext.Application["US"];


            foreach (var use in us)
            {
                if (use.Value.Username == user.Username)
                {
                    ViewBag.Mess = "User with that username already exists!";
                    return View("AddMenadzer");
                }
            }
            if (user.Username != "" && user.Username.Count() < 4)
            {
                ViewBag.Mess = "Username must have at least 4 symbols!";
                return View("AddMenadzer");
            }
            if (user.Password != "" && user.Password.Count() < 4)
            {
                ViewBag.Mess = "Password must have at least 4 symbols!";
                return View("AddMenadzer");
            }

            ViewBag.Menadzer = "Menadzer is succesfully added!";
            us.Add(user.Username, user);
            ReadWrite.WriteMenadzer(user, "~/App_Data/Users.txt");
            return RedirectToAction("Index", "Aranzman");
        }

        public ActionResult SumnjiviTuristi()
        {
            Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();
            Dictionary<string, Korisnik> sumnjivi = new Dictionary<string, Korisnik>();
            users = ReadWrite.ReadUser("~/App_Data/Users.txt");

            foreach (var us in users)
            {
                if (us.Value.Canceled >= 2)
                {
                    sumnjivi[us.Key] = users[us.Key];
                }
            }
            ViewBag.Find = sumnjivi;

            return View();
        }

        public ActionResult BlokiraniTuristi(string id)
        {
            Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();
            users = ReadWrite.ReadUser("~/App_Data/Users.txt");

            if (users.ContainsKey(id))
            {
                users[id].Blocked = true;
            }

            ReadWrite.WriteEdit(users, "~/App_Data/Users.txt");

            return RedirectToAction("Index", "Aranzman");
        }

        public ActionResult BrisanjeUsera(string id)
        {
            Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();
            users = ReadWrite.ReadUser("~/App_Data/Users.txt");

            if (users.ContainsKey(id))
            {
                users[id].Deleted = true;
            }

            ReadWrite.WriteEdit(users, "~/App_Data/Users.txt");

            return RedirectToAction("Index", "Admin");
        }

      

    }
}