﻿using PR60_2016.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR60_2016.Controllers
{
    public class SignUpController : Controller
    {
        // GET: SignUp
        public ActionResult Index()
        {
            string today = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.Today = today;
            return View();
        }

        [HttpPost]
        public ActionResult SignUp(Korisnik user)
        {
            Dictionary<string, Korisnik> us = (Dictionary<string, Korisnik>)HttpContext.Application["US"];

            foreach (var use in us)
            {
                if (use.Value.Username == user.Username)
                {
                    ViewBag.Mess = "User with that username already exists!";
                    return View("Index");
                }
            }
            if (user.Username != "" && user.Username.Count() < 4)
            {
                ViewBag.Mess = "Username must have at least 4 symbols!";
                return View("Index");
            }
            if (user.Password != "" && user.Password.Count() < 4)
            {
                ViewBag.Mess = "Password must have at least 4 symbols!";
                return View("Index");
            }

            us.Add(user.Username, user);
            ReadWrite.WriteTurista(user, "~/App_Data/Users.txt");
            Session["US"] = user;
            return RedirectToAction("Index", "LogIn");
        }
    }

}
