﻿using PR60_2016.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR60_2016.Controllers
{
    public class LogInController : Controller
    {
        // GET: LogIn
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LogIn(string username, string password)
        {
            Dictionary<string, Korisnik> users = ReadWrite.ReadUser("~/App_Data/Users.txt");
            Korisnik us = new Korisnik();

            foreach (var it in users.Values)
            {
                if (it.Password == password && it.Username == username && it.Deleted == false && it.Blocked == false)
                {
                    us = users[it.Id];
                }
            }

            if (us.Username == "")
            {
                ViewBag.UserMessage = "User not found!";
                return View("Index");
            }

            else
            {
                ViewBag.Message = "welcome";
                us.LoggedIn = true;
                Session["US"] = us;
                return RedirectToAction("Index", "Aranzman");
            }
        }

        public ActionResult LogOff()
        {
            Korisnik user = (Korisnik)Session["US"];
            user.Logoff();
            Session["US"] = null;

            return View("Index");
        }
    }
}