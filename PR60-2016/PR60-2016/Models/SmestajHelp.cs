﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR60_2016.Models
{
    public class SmestajHelp
    {

        public SmestajHelp(string id, TipSmestaja tipS, string naziv, int brojZvezdica, bool bazen, bool spaCentar, bool osobeSaInvaliditetom, bool wifi,string nazivJedinice)
        {
            Id = Id;
            TipS = tipS;
            Naziv = naziv;
            BrojZvezdica = brojZvezdica;
            Bazen = bazen;
            SpaCentar = spaCentar;
            OsobeSaInvaliditetom = osobeSaInvaliditetom;
            Wifi = wifi;
            NazivJedinice = nazivJedinice;
        }

        public string Id { get; set; }
        public TipSmestaja TipS { get; set; }
        public string Naziv { get; set; }
        public int BrojZvezdica { get; set; } 
        public bool Bazen { get; set; }
        public bool SpaCentar { get; set; }
        public bool OsobeSaInvaliditetom { get; set; }
        public bool Wifi { get; set; }
        public string NazivJedinice { get; set; }

        public SmestajHelp()
        {

        }

    }
}