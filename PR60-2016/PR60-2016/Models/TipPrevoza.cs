﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR60_2016.Models
{
    public enum TipPrevoza
    {
        UNDEFINED,
        AUTOBUS,
        AVION,
        AUTOBUSIAVION,
        INDIVIDUALAN,
        OSTALO
    }
}