﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR60_2016.Models
{
    public class AranzmanHelp
    {

        public AranzmanHelp(string id, string naziv, TipAranzmana tipA, TipPrevoza tipP, string lokacija, DateTime dateStart, DateTime dateEnd, MestoNalazenja adresa, string vreme, int maxBrojPutnika, string opis, string programPutovanja, string poster, Smestaj smestaj,string place,string nazivSmestaja)
        {
            Id = id;
            Naziv = naziv;
            TipA = tipA;
            TipP = tipP;
            Lokacija = lokacija;
            DateStart = dateStart;
            DateEnd = dateEnd;
            Adresa = adresa;
            Vreme = vreme;
            MaxBrojPutnika = maxBrojPutnika;
            Opis = opis;
            ProgramPutovanja = programPutovanja;
            Poster = poster;
            Smestaj = smestaj;
            Place = place;
            NazivSmestaja = nazivSmestaja;
        }
        public string Id { get; set; }
        public string Naziv { get; set; }
        public TipAranzmana TipA { get; set; }
        public TipPrevoza TipP { get; set; }
        public string Lokacija { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public MestoNalazenja Adresa { get; set; }
        public string Vreme { get; set; }
        public int MaxBrojPutnika { get; set; }
        public string Opis { get; set; }
        public string ProgramPutovanja { get; set; }
        public string Poster { get; set; }
        public Smestaj Smestaj { get; set; }
        public string Place { get; set; }
        public string NazivSmestaja { get; set; }

        public AranzmanHelp()
        {

        }
    }
}