﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR60_2016.Models
{
    public class SearchAranzmanHelp
    {
        public SearchAranzmanHelp(DateTime dateMin1, DateTime dateMin2, DateTime dateMax1, DateTime dateMax2, TipPrevoza tipP, TipAranzmana tipA,string naziv)
        {
            DateMin1 = dateMin1;
            DateMin2 = dateMin2;
            DateMax1 = dateMax1;
            DateMax2 = dateMax2;
            TipP = tipP;
            TipA = tipA;
            Naziv = naziv;
        }

        public DateTime DateMin1 { get; set; }
        public DateTime DateMin2 { get; set; }
        public DateTime DateMax1 { get; set; }
        public DateTime DateMax2 { get; set; }
        public TipPrevoza TipP { get; set; }
        public TipAranzmana TipA { get; set; }
        public string Naziv { get; set; }

        public SearchAranzmanHelp() { }
    }
}