﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR60_2016.Models
{
    public class Rezervacija
    {
        public Rezervacija(string id, string turista, StatusRezervacije status, string aranzmann, string jedinicaa)
        {
            Id = id;
            Turista = turista;
            Status = status;
            Aranzmann = aranzmann;
            Jedinicaa = jedinicaa;
        }

        public Rezervacija()
        {

        }

        public string Id { get; set; }
        public string Turista { get; set; }
        public StatusRezervacije Status { get; set; }
        public string Aranzmann { get; set; }
        public string Jedinicaa { get; set; }
    }
}