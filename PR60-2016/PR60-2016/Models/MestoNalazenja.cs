﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR60_2016.Models
{
    public class MestoNalazenja
    {
        public MestoNalazenja(string id, string place)
        {
            Id = id;
            Place = place;
        }
        public MestoNalazenja() { }

        public string Id { get; set; }
        public string Place { get; set; }
    }
}