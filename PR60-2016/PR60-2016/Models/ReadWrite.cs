﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace PR60_2016.Models
{
    public class ReadWrite
    {
        public static Dictionary<string, Korisnik> ReadUser(string path)
        {
            Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();
            path = HostingEnvironment.MapPath(path);

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.EndOfStream == false)
                {
                    string[] spl = sr.ReadLine().Split('|');

                    if (spl[0] != "")
                    {
                        Korisnik user = new Korisnik();
                        string k = spl[0];
                        user.Id = spl[0];
                        user.Ime = spl[1];
                        user.Prezime = spl[2];
                        user.Username = spl[3];
                        user.Password = spl[4];
                        user.Pol = spl[5];
                        user.Email = spl[6];


                        user.BirthDate = DateTime.Parse(spl[7]);


                        switch (spl[8])
                        {
                            case "administrator":
                                user.Role = UserRole.ADMINISTRATOR;
                                break;
                            case "gost":
                                user.Role = UserRole.GOST;
                                break;
                            case "menadzer":
                                user.Role = UserRole.MENADZER;
                                break;
                            case "turista":
                                user.Role = UserRole.TURISTA;
                                break;
                            default:
                                break;
                        }



                        switch (spl[9])
                        {
                            case "False":
                                user.Deleted = false;
                                break;
                            case "True":
                                user.Deleted = true;
                                break;
                            default:
                                break;
                        }
                        user.Canceled = Int32.Parse(spl[10]);
                        switch (spl[11])
                        {
                            case "False":
                                user.Blocked = false;
                                break;
                            case "True":
                                user.Blocked = true;
                                break;
                            default:
                                break;
                        }

                        user.Rezervacija = spl[12];
                        user.Aranzmans = spl[13];

                        users.Add(k, user);
                    }
                }
            }
            return users;
        }

        public static void WriteTurista(Korisnik user, string path)
        {

            path = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path, append: true))
            {
                writer.WriteLine();
                writer.Write(Guid.NewGuid() + "|" + user.Ime + "|" + user.Prezime + "|" + user.Username + "|" + user.Password + "|" + user.Pol + "|" + user.Email + "|" + user.BirthDate + "|" + "turista" + "|" + "False" + "|" + 0 + "|" + "False" + "|" + "|");
                writer.Close();
            }
        }

        public static void WriteMenadzer(Korisnik user, string path)
        {

            path = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path, append: true))
            {
                writer.WriteLine();
                writer.Write(Guid.NewGuid() + "|" + user.Ime + "|" + user.Prezime + "|" + user.Username + "|" + user.Password + "|" + user.Pol + "|" + user.Email + "|" + user.BirthDate + "|" + "menadzer" + "|" + "False" + "|" + 0 + "|" + "False" + "|" + "|");
                writer.Close();
            }
        }

        public static void WriteAranzman(Aranzman aranzman, string path, string id)
        {
            path = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path, append: true))
            {
                writer.WriteLine();
                writer.Write(Guid.NewGuid() + "|" + aranzman.Naziv + "|" + aranzman.TipA.ToString().ToLower() + "|" + aranzman.TipP.ToString().ToLower() + "|" + aranzman.Lokacija + "|" + aranzman.DateStart + "|" + aranzman.DateEnd + "|" + aranzman.Adresa.Id + "|" + aranzman.Vreme + "|" + aranzman.MaxBrojPutnika + "|" + aranzman.Opis + "|" + aranzman.ProgramPutovanja + "|" + aranzman.Poster + "|" + aranzman.Smestaj.Id + "|" + "False" + "|" + "False" + "|" + id);

                writer.Close();
            }

        }

        public static Dictionary<string, Aranzman> ReadAranzman(string path)
        {
            Dictionary<string, Aranzman> aranzmans = new Dictionary<string, Aranzman>();
            Dictionary<string, MestoNalazenja> places = new Dictionary<string, MestoNalazenja>();
            Dictionary<string, Smestaj> smestaji = new Dictionary<string, Smestaj>();
            path = HostingEnvironment.MapPath(path);

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.EndOfStream == false)
                {
                    string[] spl = sr.ReadLine().Split('|');

                    if (spl[0] != "")
                    {
                        Aranzman ar = new Aranzman();
                        MestoNalazenja mn = new MestoNalazenja();
                        Smestaj s = new Smestaj();
                        string e = spl[0];
                        ar.Id = spl[0];
                        ar.Naziv = spl[1];
                        switch (spl[2])
                        {

                            case "undefined":
                                ar.TipA = TipAranzmana.UNDEFINED;
                                break;

                            case "polupansion":
                                ar.TipA = TipAranzmana.POLUPANSION;
                                break;

                            case "punpansion":
                                ar.TipA = TipAranzmana.PUNPANSION;
                                break;

                            case "nocenjesadoruckom":
                                ar.TipA = TipAranzmana.NOCENJESADORUCKOM;
                                break;

                            case "allinclusive":
                                ar.TipA = TipAranzmana.ALLINCLUSIVE;
                                break;
                            case "najam":
                                ar.TipA = TipAranzmana.NAJAM;
                                break;

                        }
                        switch (spl[3])
                        {
                            case "undefined":
                                ar.TipP = TipPrevoza.UNDEFINED;
                                break;

                            case "autobus":
                                ar.TipP = TipPrevoza.AUTOBUS;
                                break;

                            case "autobusiavion":
                                ar.TipP = TipPrevoza.AUTOBUSIAVION;
                                break;

                            case "avion":
                                ar.TipP = TipPrevoza.AVION;
                                break;

                            case "individualan":
                                ar.TipP = TipPrevoza.INDIVIDUALAN;
                                break;
                            case "ostalo":
                                ar.TipP = TipPrevoza.OSTALO;
                                break;

                        }
                        ar.Lokacija = spl[4];
                        ar.DateStart = DateTime.Parse(spl[5]);
                        ar.DateEnd = DateTime.Parse(spl[6]);
                        mn.Id = spl[7];
                        places = ReadPlace("~/App_Data/MestoNalazenja.txt");
                        if (places.ContainsKey(mn.Id))
                        {
                            mn.Place = places[mn.Id].Place;
                        }
                        ar.Adresa = mn;
                        ar.Vreme = spl[8];
                        ar.MaxBrojPutnika = Int32.Parse(spl[9]);
                        ar.Opis = spl[10];
                        ar.ProgramPutovanja = spl[11];
                        ar.Poster = spl[12];
                        s.Id = spl[13];
                        smestaji = ReadSmestaj("~/App_Data/Smestaj.txt");
                        if (smestaji.ContainsKey(s.Id))
                        {
                            s.Naziv = smestaji[s.Id].Naziv;
                            s.TipS = smestaji[s.Id].TipS;
                            s.Bazen = smestaji[s.Id].Bazen;
                            s.OsobeSaInvaliditetom = smestaji[s.Id].OsobeSaInvaliditetom;
                            s.Wifi = smestaji[s.Id].Wifi;
                            s.SpaCentar = smestaji[s.Id].SpaCentar;
                            s.BrojZvezdica = smestaji[s.Id].BrojZvezdica;

                        }
                        ar.Smestaj = s;
                        switch (spl[14])
                        {
                            case "False":
                                ar.Deleted = false;
                                break;
                            case "True":
                                ar.Deleted = true;
                                break;
                            default:
                                break;
                        }

                        switch (spl[15])
                        {
                            case "False":
                                ar.Rezervisano = false;
                                break;
                            case "True":
                                ar.Rezervisano = true;
                                break;
                            default:
                                break;
                        }
                        ar.MenadzerId = spl[16];



                        aranzmans.Add(e, ar);
                    }
                }
            }
            return aranzmans;
        }

        public static Dictionary<string, MestoNalazenja> ReadPlace(string path)
        {
            Dictionary<string, MestoNalazenja> places = new Dictionary<string, MestoNalazenja>();

            path = HostingEnvironment.MapPath(path);

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.EndOfStream == false)
                {
                    string[] spl = sr.ReadLine().Split('|');


                    if (spl[0] != "")
                    {
                        MestoNalazenja mn = new MestoNalazenja();
                        string m = spl[0];
                        mn.Id = spl[0];
                        mn.Place = spl[1];

                        places.Add(m, mn);
                    }
                }
            }
            return places;
        }

        public static Dictionary<string, Smestaj> ReadSmestaj(string path)
        {
            Dictionary<string, Smestaj> smestaji = new Dictionary<string, Smestaj>();
            Dictionary<string, SmestajnaJedinica> jedinica = new Dictionary<string, SmestajnaJedinica>();
            path = HostingEnvironment.MapPath(path);

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.EndOfStream == false)
                {
                    string[] spl = sr.ReadLine().Split('|');


                    if (spl[0] != "")
                    {
                        Smestaj s = new Smestaj();
                        SmestajnaJedinica sj = new SmestajnaJedinica();
                        string m = spl[0];
                        s.Id = spl[0];
                        switch (spl[1])
                        {
                            case "hotel":
                                s.TipS = TipSmestaja.HOTEL;
                                break;
                            case "motel":
                                s.TipS = TipSmestaja.MOTEL;
                                break;
                            case "vila":
                                s.TipS = TipSmestaja.VILA;
                                break;
                            default:
                                break;
                        }
                        s.Naziv = spl[2];
                        s.BrojZvezdica = Int32.Parse(spl[3]);
                        switch (spl[4])
                        {
                            case "False":
                                s.Bazen = false;
                                break;
                            case "True":
                                s.Bazen = true;
                                break;
                            default:
                                break;
                        }
                        switch (spl[5])
                        {
                            case "False":
                                s.SpaCentar = false;
                                break;
                            case "True":
                                s.SpaCentar = true;
                                break;
                            default:
                                break;
                        }
                        switch (spl[6])
                        {
                            case "False":
                                s.OsobeSaInvaliditetom = false;
                                break;
                            case "True":
                                s.OsobeSaInvaliditetom = true;
                                break;
                            default:
                                break;
                        }
                        switch (spl[7])
                        {
                            case "False":
                                s.Wifi = false;
                                break;
                            case "True":
                                s.Wifi = true;
                                break;
                            default:
                                break;
                        }
                        switch (spl[8])
                        {
                            case "False":
                                s.Deleted = false;
                                break;
                            case "True":
                                s.Deleted = true;
                                break;
                            default:
                                break;
                        }

                        sj.Id = spl[9];
                        jedinica = ReadSmestajnaJedinica("~/App_Data/SmestajnaJedinica.txt");
                        if (jedinica.ContainsKey(sj.Id))
                        {
                            sj.Naziv = jedinica[sj.Id].Naziv;
                            sj.Cena = jedinica[sj.Id].Cena;
                            sj.Ljubimci = jedinica[sj.Id].Ljubimci;
                            sj.MaxGostiju = jedinica[sj.Id].MaxGostiju;
                            sj.Deleted = jedinica[sj.Id].Deleted;

                        }
                        s.Jedinica = sj;

                        smestaji.Add(m, s);
                    }
                }
            }
            return smestaji;
        }

        public static void WriteSmestaj(Smestaj smestaj, string path, string id)
        {
            path = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path, append: true))
            {
                writer.WriteLine();
                writer.Write(Guid.NewGuid() + "|" + smestaj.TipS.ToString().ToLower() + "|" + smestaj.Naziv + "|" + smestaj.BrojZvezdica + "|" + smestaj.Bazen + "|" + smestaj.SpaCentar + "|" + smestaj.OsobeSaInvaliditetom + "|" + smestaj.Wifi + "|" + "False" + "|" + smestaj.Jedinica.Id);

                writer.Close();
            }

        }

        public static void WriteEditAranzman(Dictionary<string, Aranzman> aranzmanss, string path)
        {
            string path2 = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path2))
            {
                foreach (var aranzmans in aranzmanss.Values)
                {
                    writer.WriteLine();
                    writer.Write(Guid.NewGuid() + "|" + aranzmans.Naziv + "|" + aranzmans.TipA.ToString().ToLower() + "|" + aranzmans.TipP.ToString().ToLower() + "|" + aranzmans.Lokacija + "|" + aranzmans.DateStart + "|" + aranzmans.DateEnd + "|" + aranzmans.Adresa.Id + "|" + aranzmans.Vreme + "|" + aranzmans.MaxBrojPutnika + "|" + aranzmans.Opis + "|" + aranzmans.ProgramPutovanja + "|" + aranzmans.Poster + "|" + aranzmans.Smestaj.Id + "|" + aranzmans.Deleted + "|" + aranzmans.Rezervisano + "|" + aranzmans.MenadzerId);

                }
                writer.Close();

            }
        }

        public static void WriteEdit(Dictionary<string, Korisnik> users, string path)
        {

            string path2 = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path2))
            {
                foreach (var user in users.Values)
                {
                    writer.WriteLine();
                    writer.Write(Guid.NewGuid() + "|" + user.Ime + "|" + user.Prezime + "|" + user.Username + "|" + user.Password + "|" + user.Pol + "|" + user.Email + "|" + user.BirthDate + "|" + user.Role.ToString().ToLower() + "|" + user.Deleted + "|" + user.Canceled + "|" + user.Blocked + "|" + "|"
                        );
                }
                writer.Close();
            }


        }

        public static void WriteEditSmestaj(Dictionary<string, Smestaj> smestaji, string path)
        {

            string path2 = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path2))
            {
                foreach (var smestaj in smestaji.Values)
                {
                    writer.WriteLine();
                    writer.Write(Guid.NewGuid() + "|" + smestaj.TipS.ToString().ToLower() + "|" + smestaj.Naziv + "|" + smestaj.BrojZvezdica + "|" + smestaj.Bazen + "|" + smestaj.SpaCentar + "|" + smestaj.OsobeSaInvaliditetom + "|" + smestaj.Wifi + "|" + smestaj.Deleted + "|" + smestaj.Jedinica.Id);
                }
                writer.Close();
            }


        }

        public static Dictionary<string, SmestajnaJedinica> ReadSmestajnaJedinica(string path)
        {
            Dictionary<string, SmestajnaJedinica> jedinica = new Dictionary<string, SmestajnaJedinica>();

            path = HostingEnvironment.MapPath(path);

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.EndOfStream == false)
                {
                    string[] spl = sr.ReadLine().Split('|');


                    if (spl[0] != "")
                    {
                        SmestajnaJedinica s = new SmestajnaJedinica();
                        string m = spl[0];
                        s.Id = spl[0];
                        s.Naziv = spl[1];
                        s.MaxGostiju = Int32.Parse(spl[2]);
                        switch (spl[3])
                        {
                            case "False":
                                s.Ljubimci = false;
                                break;
                            case "True":
                                s.Ljubimci = true;
                                break;
                            default:
                                break;
                        }
                        s.Cena = Int32.Parse(spl[4]);
                        switch (spl[5])
                        {
                            case "False":
                                s.Deleted = false;
                                break;
                            case "True":
                                s.Deleted = true;
                                break;
                            default:
                                break;
                        }
                        s.Smestajj = spl[6];
                        switch (spl[7])
                        {
                            case "False":
                                s.Zauzeto = false;
                                break;
                            case "True":
                                s.Zauzeto = true;
                                break;
                            default:
                                break;
                        }
                        jedinica.Add(m, s);
                    }


                }
            }
            return jedinica;
        }


        public static void WriteSmestajnaJedinica(SmestajnaJedinica smestaj, string path, string id)
        {
            path = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path, append: true))
            {
                writer.WriteLine();
                writer.Write(Guid.NewGuid() + "|" + smestaj.Naziv + "|" + smestaj.MaxGostiju + "|" + smestaj.Ljubimci + "|" + smestaj.Cena + "|" + "False" + "|" + smestaj.Smestajj + "|" + "False");

                writer.Close();
            }

        }

        public static void WriteEditSmestajnaJedinica(Dictionary<string, SmestajnaJedinica> smestaji, string path)
        {

            string path2 = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path2))
            {
                foreach (var smestaj in smestaji.Values)
                {
                    writer.WriteLine();
                    writer.Write(Guid.NewGuid() + "|" + smestaj.Naziv + "|" + smestaj.MaxGostiju + "|" + smestaj.Ljubimci + "|" + smestaj.Cena + "|"+ smestaj.Deleted + "|" + smestaj.Smestajj + "|" + smestaj.Zauzeto);
                }
                writer.Close();
            }
        }

        public static void WriteRez(Rezervacija rez, string path)
        {
            path = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path, append: true))
            {
                writer.WriteLine();
                writer.Write(Guid.NewGuid() +  "|" + rez.Turista + "|" + rez.Status + "|" + rez.Jedinicaa);
                writer.Close();

            }
        }

        public static void WriteEditRez(Dictionary<string, Rezervacija> smestaji, string path)
        {

            string path2 = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path2))
            {
                foreach (var rez in smestaji.Values)
                {
                    writer.WriteLine();
                    writer.Write(Guid.NewGuid() + "|" + rez.Turista + "|" + rez.Status + "|" + rez.Jedinicaa);
                }
                writer.Close();
            }


        }
        public static Dictionary<string, Rezervacija> ReadRez(string path)
        {
            Dictionary<string, Rezervacija> rez = new Dictionary<string, Rezervacija>();
            path = HostingEnvironment.MapPath(path);


            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.EndOfStream == false)
                {
                    string[] spl = sr.ReadLine().Split('|');

                    if (spl[0] != "")
                    {
                        Rezervacija r = new Rezervacija();
                        string e = spl[0];
                        r.Id = spl[0];
                        r.Turista = spl[1];
                        switch (spl[2])
                        {
                            case "Aktivna":
                                r.Status = StatusRezervacije.AKTIVNA;
                                break;
                            case "Otkazana":
                                r.Status = StatusRezervacije.OTKAZANA;
                                break;
                        }
                        r.Jedinicaa = spl[3];

                        rez.Add(e, r);
                    }
                }
            }
            return rez;
        }

    }
}