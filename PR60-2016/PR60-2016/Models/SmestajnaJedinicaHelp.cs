﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR60_2016.Models
{
    public class SmestajnaJedinicaHelp
    {
       

        public SmestajnaJedinicaHelp()
        {

        }

        public SmestajnaJedinicaHelp(string id,string naziv, int maxGostiju, bool ljubimci, int cena, int maxGostiju1, int minGostiju,string nazivJedinice,string idSmes)
        {
            Id = id;
            Naziv = naziv;
            MaxGostiju = maxGostiju;
            Ljubimci = ljubimci;
            Cena = cena;
            MaxGostiju1 = maxGostiju1;
            MinGostiju = minGostiju;
            IdSmes = idSmes;
        }
        public string IdSmes { get; set; }
        public string Id { get; set; }
        public string Naziv { get; set; }
        public int MaxGostiju { get; set; }
        public bool Ljubimci { get; set; }
        public int Cena { get; set; }
        public int MaxGostiju1 { get; set; }
        public int MinGostiju { get; set; }
   
    }
}