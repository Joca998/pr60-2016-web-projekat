﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR60_2016.Models
{
    public class SmestajnaJedinica
    {
        public SmestajnaJedinica(string id,string naziv,int maxGostiju, bool ljubimci, int cena,string smestajj,bool deleted,bool zauzeto)
        {
            Id = id;
            Naziv = naziv;
            MaxGostiju = maxGostiju;
            Ljubimci = ljubimci;
            Cena = cena;
            Smestajj = smestajj;
            Deleted = deleted;
            Zauzeto = zauzeto;
        }

        public SmestajnaJedinica()
        {

        }
        
        public string Id {get; set;}
        public string Naziv { get; set; }
        public int MaxGostiju { get; set; }
        public bool Ljubimci { get; set; }
        public int Cena { get; set; }
        public string Smestajj { get; set; }
        public bool Deleted;
        public bool Zauzeto { get; set; }
    }
}