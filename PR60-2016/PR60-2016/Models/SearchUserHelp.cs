﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR60_2016.Models
{
    public class SearchUserHelp
    {
        

        public SearchUserHelp() { }

        public SearchUserHelp(string ime, string prezime, UserRole role)
        {
            Ime = ime;
            Prezime = prezime;
            Role = role;
        }

        public string Ime { get; set; }
        public string Prezime { get; set; }
        public UserRole Role { get; set; }
        
    }
}