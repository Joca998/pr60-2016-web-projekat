﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR60_2016.Models
{
    public class Korisnik
    {
        public string Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Pol { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }
        public DateTime BirthDate { get; set; }
       
        public UserRole Role { get; set; }
       public string Username { get; set; }

        public bool LoggedIn { get; set; }

        public bool Deleted { get; set; }

        public int Canceled { get; set; }
        public bool Blocked { get; set; }
        public string Rezervacija { get; set; }
        public string Aranzmans { get; set; }
        public Korisnik()
        {
            Username = "";
            Password = "";
            LoggedIn = false;
        }

        public void Logoff()
        {
            Username = "";
            Password = "";
            LoggedIn = false;
        }

        public Korisnik(string username, string password)
        {
            Username = username;
            Password = password;
           
            LoggedIn = false;
        }

        public Korisnik(string id, string ime, string prezime, string password, string username, DateTime birthDate,string pol,string email, UserRole role, bool loggedIn, bool deleted, int canceled, bool blocked,string rezervacija,string aranzmans)
        {
            Id = id;
            Ime = ime;
            Prezime = prezime;
            Password = password;
            Username = username;
            Pol = pol;
            Email = email;
            BirthDate = birthDate;
            
            Role = role;
  
            LoggedIn = loggedIn;
            Deleted = deleted;
            Canceled = canceled;
            Blocked = blocked;
            Rezervacija = rezervacija;
            Aranzmans = aranzmans;
        }

        public bool Login()
        {
            if (Users.users.ContainsKey(Username) && Users.users[Username].Password.Equals(Password))
            {
                LoggedIn = true;
            }
            return LoggedIn;
        }
    }
}
